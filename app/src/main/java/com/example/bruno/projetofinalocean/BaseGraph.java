package com.example.bruno.projetofinalocean;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by BRUNO on 10/11/2016.
 */

public class BaseGraph  {

    private TextView txtVBat;
    private TextView txtVUmid;
    private TextView txtVtemp;

    private LineGraphSeries<DataPoint> series;
    private GraphView graph;
    private String titulo ="Grafico Inicial";
    private int minY = 0;
    private int maxY= 90;// Pontos maximos em y (c,bpm,h)
    private int minX=0;
    private int maxX= 1000; //Pontos maximos no tempo
    private int sizeTitulo= 20;
    private LineGraphSeries<DataPoint> seriesBat;
    private int argbTemperatura= Color.argb(250, 139, 166, 185);;
    private int argbBatimento= Color.argb(150, 201, 230, 225);



    ///TODO TESTE
    private int graphLastXValue;

///Style Suffix
//    private String suffixY="";
//    private String suffixX="";
//




    public BaseGraph(GraphView graph,
                     String titulo,int cor,
                     int maxY, int maxX, int minY, int minX) {
        this.graph = graph;

        this.titulo=titulo;

        this.minY = minY; this.maxY = maxY;
        this.minX = minX; this.maxX = maxX;
        createStyleGraph();
        createStyleTemperatura( createPoints());
        createStyleBatimento();

    }

    public BaseGraph(Activity mainActivity) {

        this.graph = (GraphView) mainActivity.findViewById(R.id.graphTemp);
        this.txtVtemp= (TextView) mainActivity.findViewById(R.id.valueTemp);
        this.txtVUmid= (TextView) mainActivity.findViewById(R.id.valueUmidade);
        this.txtVBat=(TextView) mainActivity.findViewById(R.id.valueBatimento);

        createStyleGraph();
        createStyleTemperatura(createPoints());
        createStyleBatimento();
    }


    private DataPoint[] createPoints() {

        DataPoint[] points = new DataPoint[50];
        for (int i = 0; i < points.length; i++) {
            points[i] = new DataPoint(i, Math.sin(i*0.5) * 20*(Math.random()*10+1));
        }
        return points;
    }

    /**
     * Metodo para adicionar pontos dinamicamente no grafico e no textview
     * @param listSensores
     * @param value
     */
    public void dinamicAdd(ArrayList<NoSensor> listSensores, NoSensor value){
        graphLastXValue += 1;

//          graphLastXValue =value.getTime();
//        graph.getViewport().setXAxisBoundsManual(true);
//        int size= listSensores.size();
//        if(size!=0){
//
//            graph.getViewport().setMinX(listSensores.get(0).getTime().getTime());
//            graph.getViewport().setMaxX(listSensores.get(size).getTime().getTime());
//        }
//        Calendar calendar = null;
//        Date date= new Date(value.getTime().getTime());
//
//        calendar.setTime(date);
//        calendar.getTime().m
//        Calendar calendar = Calendar.getInstance();
//        graph.getViewport().setMinX(listSensores.get(0).getTime().getTime());
//        graph.getViewport().setMaxX(value.getTime().getTime());

        series.appendData(new DataPoint(graphLastXValue, value.getTemperatura()), true, maxX);
        seriesBat.appendData(new DataPoint(graphLastXValue, value.getBatimento()), true, maxX);
//
        txtVtemp.setText((String.valueOf(value.getTemperatura()))+ "°C");
        txtVUmid.setText(String.valueOf(value.getUmidade())+ " % ");
        txtVBat.setText(String.valueOf(value.getBatimento()) + " bpm");
    }

//TODO terminar metodo
//    public void dinamicChanged(ArrayList<NoSensor> listSensores) {
//
//        for(NoSensor value:listSensores){
//            graphLastXValue += 10;
//            DataPoint aaa = new DataPoint(graphLastXValue, value.getTemperatuera());
//
////        Date date= new Date(value.getTime());
//
////        Calendar calendar = Calendar.getInstance();
////        calendar.setTime(value.getTime());
////        Date date = calendar.getTime();
//
//        series. appendData(new DataPoint(graphLastXValue, value.getTemperatuera()), true, maxX);
//        seriesBat.appendData(new DataPoint(graphLastXValue, value.getBatimento()), true, maxX);
////
//            txtVtemp.setText((String.valueOf(value.getTemperatuera())) + "°C");
//            txtVUmid.setText(String.valueOf(value.getUmidadade()) + " % ");
//            txtVBat.setText(String.valueOf(value.getBatimento()) + " bpm");
//
//        }
//        graph.addSeries(series);
//        graph.addSeries(seriesBat);
//    }

    private void createStyleTemperatura(DataPoint[] points2) {

//        this.series = new LineGraphSeries<>(points);
        this.series = new LineGraphSeries<>();

        // styling series
//        series.setTitle("Temperatura ");
//        graph.getLegendRenderer().setVisible(true);
//        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

//        int sizeListTemp = listSensores.size();
//        DataPoint[] points= new DataPoint[sizeListTemp];
//        for ( NoSensor temperatura : listSensores){
//            points[points.length]= new DataPoint(temperatura.getTime(), temperatura.getTemperatuera()) ;
//        }


        series.setDrawBackground(true); //Adiciona um backgroung na linha
        series.setColor(argbTemperatura);//Cor da linha
        series.setBackgroundColor(argbTemperatura); //Adiciona um cor de
        series.setAnimated(true);

        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10);
        series.setThickness(8);

        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                DecimalFormat fmt = new DecimalFormat("0.00");
                String sensorTemperatura = fmt.format(dataPoint.getY());
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                String tempo = format.format(dataPoint.getX());
                Log.d("Log","Data "+tempo +"\nTemperatura "+sensorTemperatura);
                Toast.makeText(graph.getContext(), "Data "+tempo +"\nTemperatura "+sensorTemperatura, Toast.LENGTH_SHORT).show();
            }
        });
        graph.addSeries(series);
    }

    public void createStyleBatimento() {

//        int sizeListSensores= listSensores.size();
//        DataPoint[] points = new DataPoint[sizeListSensores];
//        for (int i = 0; i < points.length; i++) {
//            points[i] = new DataPoint(i, i+2);
//        }

        seriesBat = new LineGraphSeries<>();
//        seriesBat.setTitle("Batimento Cardíaco ");

        seriesBat.setDrawDataPoints(true);
        seriesBat.setDataPointsRadius(10);
        seriesBat.setThickness(8);

        seriesBat.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                DecimalFormat fmt = new DecimalFormat("0.00");
                String sensorTemperatura = fmt.format(dataPoint.getY());
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                String tempo = format.format(dataPoint.getX());
                Log.d("Log","Tempo "+tempo +"\nBatimento Cardiaco "+sensorTemperatura);
                Toast.makeText(graph.getContext(), "Data "+tempo +"\nBatimento Cardiaco "+sensorTemperatura, Toast.LENGTH_SHORT).show();
            }
        });

        seriesBat.setDrawBackground(true); //Adiciona um backgroung na linha
        seriesBat.setColor(argbBatimento);//Cor da linha
        seriesBat.setBackgroundColor(argbBatimento); //Adiciona um cor de
        seriesBat.setAnimated(true);
        graph.addSeries(seriesBat);

    }


//TODO remover
//    public String getSuffixY() {
//        return suffixY;
//    }
//
//    public void setSuffixY(String suffixY) {
//        this.suffixY = suffixY;
//        setSuffix();
//    }
//
//    public String getSuffixX() {
//        return suffixX;
//    }
//
//    public void setSuffixX(String suffixX) {
//        this.suffixX = suffixX;
//        setSuffix();
//    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
        graph.setTitle(titulo);
    }

    public LineGraphSeries<DataPoint> getSeries() {
        return series;
    }

    public void setSeries(LineGraphSeries<DataPoint> series) {
        this.series = series;
    }

    public int getMinY() {
        return minY;
    }

    public void setMinY(int minY) {
        this.minY = minY;
        graph.getViewport().setMinY(minY);
    }

    public int getMaxY() {
        return maxY;
    }

    public void setMaxY(int maxY) {
        this.maxY = maxY;
        graph.getViewport().setMaxY(maxY);
    }

    public int getMinX() {
        return minX;
    }

    public void setMinX(int minX) {
        this.minX = minX;
        graph.getViewport().setMinX(minX);
    }

    public int getMaxX() {
        return maxX;
    }

    public void setMaxX(int maxX) {
        this.maxX = maxX;
        graph.getViewport().setMaxX(maxX);
    }

    public int getSizeTitulo() {
        return sizeTitulo;
    }

    public void setSizeTitulo(int sizeTitulo) {
        this.sizeTitulo = sizeTitulo;
        graph.setTitleTextSize(sizeTitulo);
    }


    private void createStyleGraph() {
//

        ////////////////////Style grafico //////////
        SimpleDateFormat dt = new SimpleDateFormat("hh:mm");
//        graph.getGridLabelRenderer().setGridColor(Color.WHITE);
        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.HORIZONTAL);
//        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(graph.getContext(),dt));//,dt
//        graph.getGridLabelRenderer().setNumHorizontalLabels(3); // only 4 because of the space
        graph.getGridLabelRenderer().setHumanRounding(false);
        graph.getGridLabelRenderer().setHighlightZeroLines(false);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.BLACK); //Cor da label x
//        graph.getGridLabelRenderer().setVerticalLabelsColor(Color.WHITE);//Cor da label Y



        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(minY);
        graph.getViewport().setMaxY(maxY);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(minX);
        graph.getViewport().setMaxX(maxX);

        graph.getViewport().setScrollable(false); // enables horizontal scrolling
        graph.getViewport().setScrollableY(false); // enables vertical scrolling

        graph.getViewport().setScalable(true); // enables horizontal zooming and scrolling
        graph.getViewport().setScalableY(false); // enables vertical zooming and scrolling



    }
    //TODO remover
//    public void setSuffix(){
//        // custom label formatter to show currency "EUR"
//        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
//            @Override
//            public String formatLabel(double value, boolean isValueX) {
//                if (isValueX) {
//                    // show normal x values
//                    return super.formatLabel(value, isValueX) + " "+ suffixX;
//                } else {
//                    // show currency for y values
//                    return super.formatLabel(value, isValueX) + " "+ suffixY;
//                }
//            }
//        });
//    }
//TODO Remover
//    public void setLegendaXY(String legendaX,String legendaY){
//        //        graph.setTitle("Chart Title");
////        graph.getGridLabelRenderer().setVerticalAxisTitle("Vertical Axis");
////        graph.getGridLabelRenderer().setHorizontalAxisTitle("Horizontal Axis");
////
//        graph.getGridLabelRenderer().setHorizontalAxisTitle(legendaX);
//        graph.getGridLabelRenderer().setVerticalAxisTitle(legendaY);
//
//        // optional styles
//        //graph.setTitleTextSize(40);
//        //graph.setTitleColor(Color.BLUE);
//        //graph.getGridLabelRenderer().setVerticalAxisTitleTextSize(40);
//        graph.getGridLabelRenderer().setVerticalAxisTitleColor(Color.BLUE);
//        //graph.getGridLabelRenderer().setHorizontalAxisTitleTextSize(40);
//        graph.getGridLabelRenderer().setHorizontalAxisTitleColor(Color.BLUE);
//    }
}

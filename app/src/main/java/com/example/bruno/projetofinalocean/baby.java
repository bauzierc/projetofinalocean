package com.example.bruno.projetofinalocean;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by BRUNO on 28/12/2016.
 */



public class Baby {
    private String nome;
    private int sexoBaby;
    private Date dataNascimento;

    public Baby() {
        this.nome ="Lucas Gabriel";
        this.sexoBaby = Util.MASCULINO;
        Calendar calendar = Calendar.getInstance();
        Date d1 = calendar.getTime();
        dataNascimento= d1;
    }

    public Baby(String nome, int sexoBaby, Date dataNascimento) {
        this.nome = nome;
        this.sexoBaby = sexoBaby;
        this.dataNascimento = dataNascimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getSexoBaby() {
        return sexoBaby;
    }

    public void setSexoBaby(int sexoBaby) {
        this.sexoBaby = sexoBaby;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Override
    public String toString() {
        return "Baby{" +
                "nome='" + nome + '\'' +
                ", sexoBaby=" + sexoBaby +
                ", dataNascimento=" + dataNascimento +
                '}';
    }
}

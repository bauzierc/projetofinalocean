package com.example.bruno.projetofinalocean;

import android.app.Activity;
import android.view.ContextThemeWrapper;
import android.widget.LinearLayout;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by BRUNO on 16/11/2016.
 */


public class AlertaActivity {
    private ContextThemeWrapper styleAlerta;
    private LinearLayout layoutAlerta;

    private String textoAlerta;
    private String data;

    public AlertaActivity() {
    }


    public AlertaActivity(Activity mainActivity) {
        this.layoutAlerta = (LinearLayout) mainActivity.findViewById(R.id.lytAlerta);
        this.styleAlerta = new ContextThemeWrapper(mainActivity, R.style.stylealerta);
    }

    public AlertaActivity(String mensagem, int tipoAleta,Date dataAleta  ) {
        this.textoAlerta = mensagem;
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");

        this.data = format.format(dataAleta.getTime());
        this.icone = tipoAleta;
    }

    public String getTextoAlerta() {
        return textoAlerta;
    }

    public String getData() {
        return data;
    }

    public int getIcone() {
        return icone;
    }

    private int icone;


    private int ICONTEM = R.drawable.temperatureicon;
    private int ICONHEART = R.drawable.healthcare;
    private int ICONUMIDADE = R.drawable.umidade;


    private int count = 0;



    public AlertaActivity(LinearLayout layoutAlerta, ContextThemeWrapper styleAlerta) {
        this.layoutAlerta = layoutAlerta;
        this.styleAlerta = styleAlerta;
    }


}
package com.example.bruno.projetofinalocean;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.jjoe64.graphview.series.DataPoint;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by BRUNO on 29/11/2016.
 */

public class RecoverData {


    private AlertaActivity alertaActivity;
    private BaseGraph graphNoSensor;
    private ArrayList<NoSensor> listSensores;
    private ArrayList<AlertaActivity> listAlertaActivities;
    private Activity context;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private String dataNow;

    public RecoverData(Activity mainActivity) {
        //Recuperar compomentes do layout
            this.context = mainActivity;
            listSensores= new ArrayList<NoSensor>();
            listAlertaActivities = new ArrayList<AlertaActivity>();

            graphNoSensor = new BaseGraph(mainActivity);
            alertaActivity = new AlertaActivity(mainActivity);

            Calendar calendar = Calendar.getInstance();

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            dataNow = (format.format(calendar.getTime()));
            Log.d("DataBruno",dataNow);
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            DatabaseReference myRef = database.getReference("noSensor/Data"+dataNow);

            // Last 100 posts, these are automatically the 100 most recent
            // due to sorting by push() keys
            Query recentPostsQuery = myRef.limitToFirst(1440);//.orderByChild("time")
            recentPostsQuery.addChildEventListener(new ChildEventListener() {

                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                // A new comment has been added, add it to the displayed list
                    NoSensor value = dataSnapshot.getValue(NoSensor.class);
                    Log.d("Bruno", "onDataChange: Value is: " + value.toString());
                    String dataFirebase= value.compData();
                    if (dataNow.compareTo(dataFirebase)!=0) {
                        //TODO atualizar databaseReferece
                    }
                    listSensores.add(value);
                    graphNoSensor.dinamicAdd(listSensores,value);

                    if (getAlerta(value.getMensagem1(),value.getTipoAlerta1(),value.getTime())) ;
                    if (getAlerta(value.getMensagem2(),value.getTipoAlerta2(),value.getTime())) ;
                    if (getAlerta(value.getMensagem3(),value.getTipoAlerta3(),value.getTime())) ;


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
//                // A new comment has been added, add it to the displayed list
                    NoSensor value = dataSnapshot.getValue(NoSensor.class);
                    Log.d("Bruno", "onChildChanged: Value is: " + value.toString());
//                    if(value.getMensagem().isEmpty()){
//                        Log.d("Bruno", "onDataChange: Value is: " + value.toString());
//                        Log.d("Bruno", "Erro onDataChange: Value is: " + value.toString());
//                        return;
//                    }

                    for (NoSensor no : listSensores){
                        if(no.getTime()==value.getTime()){
                            int indexno = listSensores.indexOf(no);
                            listSensores.remove(indexno);
                            listSensores.add(value);
                        }

                    }
                    graphNoSensor.dinamicAdd(listSensores, value);
//                    alertaActivity.dinamicAdd(value);
//                }

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    Log.d("Bruno", "onChildRemoved:" + dataSnapshot.getKey());
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    Log.d("Bruno", "onChildMoved:" + dataSnapshot.getKey());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.w("Bruno", "Failed to read value.", databaseError.toException());
                }
            });

        }

    private boolean getAlerta(String mensagem, int tipoAleta,Date dataAleta) {
        if(!mensagem.isEmpty()){
            AlertaActivity alertaActivity = new AlertaActivity(mensagem, tipoAleta, dataAleta);
             listAlertaActivities.add(0, alertaActivity);
             povoarRecycleview();
        }else{
            Log.d("Bruno", "Erro onDataChange: ");
            return true;
        }
        return false;
    }


    private DataPoint[] createPoints() {

        DataPoint[] points = new DataPoint[50];
        for (int i = 0; i < points.length; i++) {
            points[i] = new DataPoint(i, Math.sin(i*0.5) * 20*(Math.random()*10+1));
        }
        return points;
    }

    public void povoarRecycleview (){

        MyAdapter adapter = new MyAdapter(this.context, listAlertaActivities);

        mRecyclerView = (RecyclerView) context.findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this.context,LinearLayoutManager.HORIZONTAL,false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mRecyclerView.setAdapter(adapter);
    }

    public ArrayList<NoSensor> getListSensores() {
        return listSensores;
    }

    public void setListSensores(ArrayList<NoSensor> listSensores) {
        this.listSensores = listSensores;
    }



}

package com.example.bruno.projetofinalocean.Remover;


import android.util.Log;

import com.example.bruno.projetofinalocean.Baby;
import com.example.bruno.projetofinalocean.NoSensor;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;

/**
 * Created by BRUNO on 29/11/2016.
 */

public class SalvarBase implements Serializable {


    public SalvarBase() {
    }

    public void newBaby(){
        Baby bebeabordo= new Baby();

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("baby");
        myRef.push().setValue(bebeabordo);
        Log.d("Bruno",bebeabordo.toString());

    }

    public void newNo(){

        NoSensor noSensor= new NoSensor();

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("noSensor");
        myRef.push().setValue(noSensor);

//        DatabaseReference myRef = database.getReference("noSensor");
//        myRef.setValue(noSensor);

//        ColecaoReceitas colecaoReceitas= new ColecaoReceitas(util.secretStorage());
//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = database.getReference("colecaoReceitas");
//        myRef.push().setValue(colecaoReceitas);
    }
}

package com.example.bruno.projetofinalocean;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.bruno.projetofinalocean.Remover.SalvarBase;
import com.google.firebase.crash.FirebaseCrash;
import com.jjoe64.graphview.GraphView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;


public class MainActivity extends AppCompatActivity {

    private boolean isCheckedTemp=false;

    private int count=0;
    private double mLastRandom;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Adiciona um novo valor do sensor
        SalvarBase salvarBase = new SalvarBase();
//        salvarBase.newNo();
//        salvarBase.newBaby();
//        FirebaseCrash.log("Activity created");
        int mLastRandom = 2;

        long unixSeconds = 1490892764;
        Date date = new Date(unixSeconds*1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z"); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+0")); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);
        System.out.println(formattedDate);
        Log.d("TimeBruno","Teste Time "+ formattedDate);

//        Calendar calendar = Calendar.getInstance();
//        TextView dataNow= (TextView) findViewById(R.id.valueDataNow);
//        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//        dataNow.setText(format.format(calendar.getTime()));


        //Recuperar valor do FireBase
        RecoverData recoverData = new RecoverData(this);



    }

//    Random mRand = new Random();
//    private double getRandom() {
//        return mLastRandom += mRand.nextInt()*0.5 - 0.25;
//    }

    public void recolherTemp(View view){
        Log.d("Temperatura","Onclicker");
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.lytTemp);

        isCheckedTemp=!isCheckedTemp;
        if(isCheckedTemp){ //Remover graph
            Log.d("Temperatura","Onclicker true");
            GraphView graphView =(GraphView) findViewById(R.id.graphTemp);
//            graphView.setLayoutParams(new GraphView.LayoutPara1ms(LayoutParams.WRAP_CONTENT,
//                    LayoutParams.WRAP_CONTENT));
//            relativeLayout.removeView(graphView);
//            relativeLayout.setLayoutTransition(30);
        }else {//Adiciona grph
            Log.d("Temperatura","Onclicker false");
//            relativeLayout.addView(addGraphTemp());
        }

    }


}

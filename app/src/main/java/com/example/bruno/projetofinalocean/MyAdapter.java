package com.example.bruno.projetofinalocean;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by BRUNO on 16/12/2016.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private final Context context;
    private ArrayList<AlertaActivity> lista;
    private  AdapterListerner listerner;



    public AdapterListerner getListerner() {
        return listerner;
    }

    public void setListerner(AdapterListerner listerner) {
        this.listerner = listerner;
    }

    public MyAdapter(Context context, ArrayList<AlertaActivity> lista) {
        this.lista = lista;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alerta, null);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Recuperei a referencia
        AlertaActivity mensagemAleta = lista.get(position);


        // Seta os valores do alerta para o layout dentro do holder
        holder
                .setTextAlerta(mensagemAleta.getTextoAlerta())
                .setDataAlerta(mensagemAleta.getData());
                holder.setImage(mensagemAleta.getIcone());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtAlerta;
        private TextView txtData;
        private TextView txtbackground;

        private ImageView imgCapa;


        public ViewHolder(View itemView) {
            super(itemView);


            // Recuperei as referencias do layout
            imgCapa = (ImageView) itemView.findViewById(R.id.valueimgAlerta);
            txtAlerta = (TextView) itemView.findViewById(R.id.valueAlerta);
            txtbackground = (TextView) itemView.findViewById(R.id.backgroundAlerta);
            txtData = (TextView) itemView.findViewById(R.id.valueData);
            itemView.setOnClickListener(this);
        }

        public ViewHolder setTextAlerta(String tipoAlerta){
            if(txtAlerta == null) return this;
            txtAlerta.setText(tipoAlerta);
            return this;
        }

        public void setDataAlerta(String dataOcorrencia){
            if(txtData == null) return;
            txtData.setText(dataOcorrencia);
        }


        public void setImage(int hasAlerta){
            if(imgCapa == null) return;

            // Processar a imagem
//            Picasso.with(context).load(image).resize(200, 200).centerCrop().into(imgCapa);
//
//            public ViewHolder setImage(String image){
//                if(imgCapa == null) return this;

            // Processaa a imagem com a biblioteca OceanLib
//            Ocean.glide(context) // Contexto da aplicacao
//                    .load(image) // URL da imagem
//                    .build(GlideRequest.BITMAP) // Constroi um pedido para processar a imagem como Bitmap
//                    //.resize(200, 200) // Recorta a imagem no tamanho especifico
//                    //.circle() // Arrendoda a imagem
//                    .into(imgCapa); // Abre a imagem no imageView passado
            switch (hasAlerta){
//
                case Util.TEMPERATURA:
                    imgCapa.setImageResource(R.drawable.temperatureicon);
                    txtbackground.setBackgroundColor(Color.parseColor("#fa8ba6b9"));
                    break;
                case Util.UMIDADE:
                    imgCapa.setImageResource( R.drawable.umidade);
                    txtbackground.setBackgroundColor(Color.parseColor("#fa6f4785"));

                    break;
                case Util.HEART:
                    imgCapa.setImageResource( R.drawable.healthcare);
                    txtbackground.setBackgroundColor(Color.parseColor("#fac9e6e1"));
                    break;

            }
        }

        @Override
        public void onClick(View v) {
            int position = getPosition();
            Log.d("Debug"," Teste Possition "+position);
            if (listerner!= null){

                listerner.onItemClick(itemView,position);
            }
        }
    }

    interface AdapterListerner {
        void onItemClick(View view, int posicao);
    }

}

package com.example.bruno.projetofinalocean;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by BRUNO on 28/12/2016.
 */

public final class Util {

    ///AlertaActivity
    public static final int NOALERTA = -1;
    public static final int TEMPERATURA = 0;
    public static final int UMIDADE = 1;
    public static final int HEART = 2;

    //Baby
    public static final int MASCULINO = 0;
    public static final int FEMININO = 1;


    public static final boolean ALERTAVISIVEL = true;
    public static final boolean ALERTAINVISIVEL = false;

    @Override
    public String toString() {
        return super.toString();
    }


    //    From String to Date
    public Date strDate() {
        String dtStart = "2010-10-15T09:27:37Z";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date = null;
        try {
             date = format.parse(dtStart);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            return date;
        }
    }

    //    From Date to String
    public String datetoStr() {

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date = new Date();
        String datetime = dateformat.format(date);
        System.out.println("Current Date Time : " + datetime);
        return datetime;

    }
} //fim da classe
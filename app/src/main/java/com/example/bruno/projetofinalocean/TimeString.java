package com.example.bruno.projetofinalocean;

/**
 * Created by BRUNO on 13/03/2017.
 */

public class TimeString {
    private String day;
    private int hours;
    private int minutes;
    private String month;
    private int seconds;
    private int year;

    public TimeString() {
    }

    public TimeString(int year, String month, String day, int hours, int minutes, int seconds  ) {
        this.day = day;
        this.hours = hours;
        this.minutes = minutes;
        this.month = month;
        this.seconds = seconds;
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "TimeString{" +
                "day=" + day +
                ", month=" + month +
                ", year=" + year +
                ", hours=" + hours +
                ", minutes=" + minutes +
                ", seconds=" + seconds +
                '}';
    }

    public String parseString() {
        return  year     +"-" +
                month   +"-" +
                day     +"T" +
                hours   +":" +
                minutes +":" +
                seconds +"Z";
    }
//    ("yyyy-MM-dd'T'HH:mm:ss'Z'");
}

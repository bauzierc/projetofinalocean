package com.example.bruno.projetofinalocean;

import android.util.Log;

import com.example.bruno.projetofinalocean.TimeString;
import com.example.bruno.projetofinalocean.Util;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by BRUNO on 29/11/2016.
 */

public class NoSensor implements Serializable {
    private int umidade;
    private double temperatura;
    private int batimento;

    private int tipoAlerta1;  private String mensagem1;
    private int tipoAlerta2;  private String mensagem2;
    private int tipoAlerta3;  private String mensagem3;


    private int segundo, hora, minuto; //Data

    private int  ano; //Hora
    private String dia, mes;
    private TimeString time;
    private Util util= new Util();

    public String getDia() { return dia;}
    public int getHora() { return hora;}
    public int getMinuto() { return minuto;}
    public String getMes() { return mes;}
    public int getSegundo() {return segundo;}
    public int getAno() {return ano;}


    public int getUmidade() {
        return umidade;
    }
    public double getTemperatura() { return temperatura; }
    public int getBatimento() {
        return batimento;
    }

    public int getTipoAlerta1() { return tipoAlerta1; }
    public String getMensagem1() { return mensagem1; }
    public int getTipoAlerta2() { return tipoAlerta2; }
    public String getMensagem2() { return mensagem2; }
    public int getTipoAlerta3() {return tipoAlerta3; }
    public String getMensagem3() { return mensagem3; }


    public Date getTime() {
        time = new TimeString(getAno(),getMes(),getDia(),getHora(),getMinuto(),getSegundo());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date = null;
        try {
            date = format.parse(time.parseString());
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }finally {
            return date;
        }
    }

    public String compData(){
        String data = null;
       data= getAno()+"-"+getMes()+"-"+getDia();
        return data;
    }


//    public void setTime(Date time) {
//        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
//        String datetime = dateformat.format(time);
//        System.out.println("Current Date Time : " + datetime);
//        this.time = datetime;
//    }


//            alerta.createAlerta(TEMPERATURA," Bebê com febre",currentDateTimeString);
    //        alerta.createAlerta(UMIDADE,"Bebê esta urinado",currentDateTimeString);
    //        alerta.createAlerta(HEART,"Coração acelerado",currentDateTimeString);

    public NoSensor() {
//        umidadade=0;
//        temperatuera=37.7;
//        batimento=90;
//        tipoalerta=util.HEART;
//        setStatusAlerta(util.ALERTAVISIVEL);
//        mensagem="Coração acelerado";
//        Date data =new Date();
//        Calendar calendar = Calendar.getInstance();
//        Date d1 = calendar.getTime();
//        time= d1;
//        inicializar();
//        Log.d("Bruno", "Construtor ");


    }

    @Override
    public String toString() {
        return "NoSensor{" +
                "umidade=" + getUmidade() +
                ", temperatura=" + getTemperatura() +
                ", batimento=" + getBatimento() +
                ", dia=" + getDia() +
                ", horas=" + getHora() +
                ", minutos=" + getMinuto() +
                ", mês=" + getMes() +
                ", segundo=" + getSegundo() +
                ", ano=" + getAno() +
                ", tipoalerta1=" + tipoAlerta1 +
                ", mensagem1='" + mensagem1 + '\'' +
                ", tipoalerta2=" + tipoAlerta2 +
                ", mensagem2='" + mensagem2 + '\'' +
                ", tipoalerta3=" + tipoAlerta3 +
                ", mensagem3='" + mensagem3 + '\'' +
                '}';
    }
}
